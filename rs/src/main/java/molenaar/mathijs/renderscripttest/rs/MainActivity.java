package molenaar.mathijs.renderscripttest.rs;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;

import java.util.ArrayList;


public class MainActivity extends Activity {
    /* Number of bitmaps that is used for renderScript thread and UI thread synchronization.
       Ideally, this can be reduced to 2, however in some devices, 2 buffers still showing tierings on UI.
       Investigating a root cause.
     */
    private final int NUM_BITMAPS = 3;
    private int mCurrentBitmap = 0;
    private Bitmap mBitmapIn;
    private Bitmap[] mBitmapsOut;
    private ImageView mImageView;

    private RenderScript mRS;
    private Allocation mInAllocation;
    private Allocation[] mOutAllocations;
    private ScriptC_rotation mRotationScript;
    private ScriptC_saturation mSaturationScript;
    private ScriptGroup mScriptGroup;

    private float mRotation;
    private float mSaturation;

    private int mImageWidth = 480;
    private int mImageHeight = 400;

    private boolean mUseRS = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        /*
         * Initialize UI
         */
        mBitmapIn = loadBitmap(R.drawable.data);
        mBitmapsOut = new Bitmap[NUM_BITMAPS];
        for (int i = 0; i < NUM_BITMAPS; ++i) {
            mBitmapsOut[i] = Bitmap.createBitmap(mBitmapIn.getWidth(),
                    mBitmapIn.getHeight(), mBitmapIn.getConfig());
        }

        mImageView = (ImageView) findViewById(R.id.imageView);
        mImageView.setImageBitmap(mBitmapsOut[mCurrentBitmap]);
        mCurrentBitmap += (mCurrentBitmap + 1) % NUM_BITMAPS;

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

        ArrayList<String> itemList = new ArrayList<String>();
        itemList.add("Java");
        itemList.add("Renderscript");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                itemList);
        actionBar.setListNavigationCallbacks(adapter, new ActionBar.OnNavigationListener() {
            @Override
            public boolean onNavigationItemSelected(int index, long id) {
                if (index == 0)
                    mUseRS = false;
                else
                    mUseRS = true;

                return true;
            }
        });


        SeekBar rotationSeekbar = (SeekBar) findViewById(R.id.rotationSeekbar);
        rotationSeekbar.setProgress(0);
        rotationSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                float max = 360.0f;
                float min = 0.0f;
                mRotation = (float) ((max - min) * (progress / 100.0) + min);
                updateImage(mRotation, mSaturation);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        SeekBar saturationSeekbar = (SeekBar) findViewById(R.id.saturationSeekbar);
        saturationSeekbar.setProgress(100);
        saturationSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                float max = 1.0f;
                float min = 0.0f;
                mSaturation = (float) ((max - min) * (progress / 100.0) + min);
                updateImage(mRotation, mSaturation);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        /*
         * Create renderScript
         */
        createScript();

        /*
         * Invoke renderScript kernel and update imageView
         */
        updateImage(0.0f, 1.0f);
    }

    /*
     * Initialize RenderScript
     * In the sample, it creates RenderScript kernel that performs saturation manipulation.
     */
    private void createScript() {
        //Initialize RS
        mRS = RenderScript.create(this);

        //Allocate buffers
        mInAllocation = Allocation.createFromBitmap(mRS, mBitmapIn);
        mOutAllocations = new Allocation[NUM_BITMAPS];
        for (int i = 0; i < NUM_BITMAPS; ++i) {
            mOutAllocations[i] = Allocation.createFromBitmap(mRS, mBitmapsOut[i]);
        }

        //Load script
        mRotationScript = new ScriptC_rotation(mRS);
        mRotationScript.set_imageWidth(mImageWidth);
        mRotationScript.set_imageHeight(mImageHeight);

        mSaturationScript = new ScriptC_saturation(mRS);

        ScriptGroup.Builder builder = new ScriptGroup.Builder(mRS);
        builder.addKernel(mSaturationScript.getKernelID_saturation());
        builder.addKernel(mRotationScript.getKernelID_rotation());
        builder.addConnection(mInAllocation.getType(),
                mSaturationScript.getKernelID_saturation(),
                mRotationScript.getFieldID_input());
        mScriptGroup = builder.create();
    }

    /*
     * In the AsyncTask, it invokes RenderScript intrinsics to do a filtering.
     * After the filtering is done, an operation blocks at Allication.copyTo() in AsyncTask thread.
     * Once all operation is finished at onPostExecute() in UI thread, it can invalidate and update ImageView UI.
     */
    private class RenderScriptTask extends AsyncTask<Float, Integer, Integer> {
        Boolean issued = false;

        protected Integer doInBackground(Float... values) {
            int index = -1;
            if (isCancelled() == false) {
                issued = true;
                index = mCurrentBitmap;

                mRotationScript.set_rotationAngle(values[0]);
                mRotationScript.invoke_calculateMatrix();
                mSaturationScript.set_saturationValue(values[1]);

                mScriptGroup.setInput(mSaturationScript.getKernelID_saturation(),
                        mInAllocation);
                mScriptGroup.setOutput(mRotationScript.getKernelID_rotation(),
                        mOutAllocations[index]);
                mScriptGroup.execute();

                mOutAllocations[index].copyTo(mBitmapsOut[index]);
                mCurrentBitmap = (mCurrentBitmap + 1) % NUM_BITMAPS;
            }
            return index;
        }

        void updateView(Integer result) {
            if (result != -1) {
                // Request UI update
                mImageView.setImageBitmap(mBitmapsOut[result]);
                mImageView.invalidate();
            }
        }

        protected void onPostExecute(Integer result) {
            updateView(result);
        }

        protected void onCancelled(Integer result) {
            if (issued) {
                updateView(result);
            }
        }
    }

    private class JavaTask extends AsyncTask<Float, Integer, Integer> {
        Boolean issued = false;

        @Override
        protected Integer doInBackground(Float... values) {
            int index = -1;
            if (!isCancelled()) {
                issued = true;
                index = mCurrentBitmap;

                float rotationAngle = values[0];
                float saturation = values[1];

                int[] origArgb = new int[mImageWidth * mImageHeight];
                int[] argb = new int[mImageWidth * mImageHeight];

                /* Convert bitmap to pixel array */
                mBitmapIn.getPixels(origArgb, 0, mImageWidth, 0, 0, mImageWidth, mImageHeight);

                /* Calculate saturation */
                int R, G, B;
                float result;
                for (int y = 0; y < mImageHeight; y++) {
                    int offset = y * mImageWidth;
                    for (int x = 0; x < mImageWidth; x++) {
                        R = Color.red(origArgb[offset + x]);
                        G = Color.green(origArgb[offset + x]);
                        B = Color.blue(origArgb[offset + x]);

                        result = R * 0.299f + G * 0.587f + B * 0.114f;

                        R = (int) (result + (R - result) * saturation);
                        G = (int) (result + (G - result) * saturation);
                        B = (int) (result + (B - result) * saturation);

                        origArgb[offset + x] = Color.rgb(R, G, B);
                    }
                }

                float sinus = (float) Math.sin(Math.toRadians(rotationAngle));
                float cosin = (float) Math.cos(Math.toRadians(rotationAngle));
                int cX = mImageWidth / 2;
                int cY = mImageHeight / 2;

                for (int y = 0; y < mImageHeight; y++) {
                    int offset = y * mImageWidth;
                    int sY = y - cY;
                    for (int x = 0; x < mImageWidth; x++) {
                        int sX = x - cX;

                        int origX = (int) Math.round(cosin * sX + sinus * sY) + cX;
                        int origY = (int) Math.round(-sinus * sX + cosin * sY) + cY;

                        if (origX > 0 && origX < mImageWidth && origY > 0 && origY < mImageHeight)
                            argb[offset + x] = origArgb[origY * mImageWidth + origX];
                        else
                            argb[offset + x] = Color.rgb(255, 255, 255);
                    }
                }

                mBitmapsOut[index] = Bitmap.createBitmap(argb, mImageWidth, mImageHeight, Bitmap.Config.ARGB_8888);
                mCurrentBitmap = (mCurrentBitmap + 1) % NUM_BITMAPS;
            }
            return index;
        }

        void updateView(Integer result) {
            if (result != -1) {
                // Request UI update
                mImageView.setImageBitmap(mBitmapsOut[result]);
                mImageView.invalidate();
            }
        }

        protected void onPostExecute(Integer result) {
            updateView(result);
        }

        protected void onCancelled(Integer result) {
            if (issued) {
                updateView(result);
            }
        }
    }

    AsyncTask<Float, Integer, Integer> currentTask = null;

    /*
    Invoke AsynchTask and cancel previous task.
    When AsyncTasks are piled up (typically in slow device with heavy kernel),
    Only the latest (and already started) task invokes RenderScript operation.
     */
    private void updateImage(final float rotation, final float saturation) {
        if (currentTask != null)
            currentTask.cancel(false);

        if (mUseRS)
            currentTask = new RenderScriptTask();
        else
            currentTask = new JavaTask();

        mRotation = rotation;
        mSaturation = saturation;

        currentTask.execute(rotation, saturation);
    }

    /*
    Helper to load Bitmap from resource
     */
    private Bitmap loadBitmap(int resource) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeResource(getResources(), resource, options);
    }

}
